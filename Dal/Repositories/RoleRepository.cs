﻿using Dal.Context;
using Dal.Entities;
using Dal.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Dal.Repositories
{
    /// <summary>
    /// Role repository
    /// </summary>
    public class RoleRepository : Repository<Role, UserContext>, IRoleRepository
    {
        public RoleRepository(UserContext context) : base(context) { }
        public IQueryable<Role> GetAllWithDetails()
        {
            return _context.Roles.Include(i => i.Users);
        }

        public async Task<Role> GetByIdWithDetailsAsync(int id)
        {
            return await _context.Roles.Include(i => i.Users).FirstOrDefaultAsync(i=>i.Id==id);
        }
    }
}

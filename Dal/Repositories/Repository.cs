﻿using Dal.Entities;
using Dal.Interfaces;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Dal.Repositories
{
    /// <summary>
    /// Generic repository implementation
    /// </summary>
    /// <typeparam name="TEntity">base entity model</typeparam>
    /// <typeparam name="TContext">db context</typeparam>
    public class Repository<TEntity, TContext> : IRepository<TEntity> 
        where TEntity : BaseEntity
        where TContext:DbContext
    {
        protected readonly TContext _context;
        protected readonly DbSet<TEntity> _dbSet;
        public Repository(TContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public TEntity Add(TEntity entity)
        {
            return _dbSet.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            _dbSet.Remove(await _dbSet.FindAsync(id));
        }

        public IQueryable<TEntity> FindAll()
        {
            return _dbSet.AsNoTracking();
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public void Update(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}

﻿using Dal.Context;
using Dal.Entities;
using Dal.Interfaces;

namespace Dal.Repositories
{
    /// <summary>
    /// Employers In Summaries repository
    /// </summary>
    public class EmployersInSummariesRepository : Repository<EmployersInSummaries, ApplicationContext>, IEmployersInSummariesRepository
    {
        public EmployersInSummariesRepository(ApplicationContext context) : base(context) { }
    }
}

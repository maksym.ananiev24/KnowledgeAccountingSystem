﻿using Dal.Context;
using Dal.Entities;
using Dal.Interfaces;

namespace Dal.Repositories
{
    /// <summary>
    /// Position repository
    /// </summary>
    public class PositionRepository:Repository<Position,ApplicationContext>,IPositionRepository
    {
        public PositionRepository(ApplicationContext context) : base(context) { }
    }
}

﻿using Dal.Context;
using Dal.Entities;
using Dal.Interfaces;

namespace Dal.Repositories
{
    /// <summary>
    /// Education level repository
    /// </summary>
    public class EducationLevelRepository:Repository<EducationLevel,ApplicationContext>,IEducationLevelRepository
    {
        public EducationLevelRepository(ApplicationContext context) : base(context) { }
    }
}

﻿using Dal.Context;
using Dal.Entities;
using Dal.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Dal.Repositories
{
    /// <summary>
    /// Summary repository
    /// </summary>
    public class SummaryRepository:Repository<Summary,ApplicationContext>,ISummaryRepository
    {
        public SummaryRepository(ApplicationContext context) : base(context) { }

        public IQueryable<Summary> GetAllWithDetails()
        {
            return _context.Summaries.Include(i => i.Educations.Select(y=>y.EducationLevel)).Include(i => i.WorkExperiences).Include(i => i.EmployersId).Include(i=>i.Position).Include(i=>i.EmployersId);
        }

        public async Task<Summary> GetByIdWithDetailsAsync(int id)
        {
            return await GetAllWithDetails().FirstOrDefaultAsync(i => i.Id == id);
        }
    }
}

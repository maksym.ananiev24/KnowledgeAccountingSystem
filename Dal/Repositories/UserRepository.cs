﻿using Dal.Context;
using Dal.Entities;
using Dal.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Dal.Repositories
{
    /// <summary>
    /// User repository
    /// </summary>
    public class UserRepository:Repository<User,UserContext>, IUserRepository
    {
        public UserRepository(UserContext context) : base(context) { }

        public IQueryable<User> GetAllWithDetails()
        {
            return _context.Users.Include(i=>i.Role);
        }

        public async Task<User> GetByIdWithDetailsAsync(int id)
        {
            return await _context.Users.Include(i => i.Role).FirstOrDefaultAsync(i => i.Id == id);
        }

        public async Task<User> GetUserByEmail(string email)
        {
            return await _context.Users.Include(i => i.Role).FirstOrDefaultAsync(i => i.Email == email);
        }
    }
}

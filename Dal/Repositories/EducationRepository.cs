﻿using Dal.Context;
using Dal.Entities;
using Dal.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Dal.Repositories
{
    /// <summary>
    /// Education repository
    /// </summary>
    public class EducationRepository : Repository<Education, ApplicationContext>, IEducationRepository
    {
        public EducationRepository(ApplicationContext context) : base(context) { }

        public IQueryable<Education> GetAllWithDetails()
        {
            return _context.Educations.Include(i => i.EducationLevel).Include(i => i.Summaries);
        }

        public async Task<Education> GetByIdWithDetailsAsync(int id)
        {
            return await _context.Educations.Include(i => i.EducationLevel).Include(i => i.Summaries).FirstOrDefaultAsync(i=>i.Id==id);
        }
    }
}

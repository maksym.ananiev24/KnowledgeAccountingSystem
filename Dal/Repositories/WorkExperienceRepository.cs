﻿using Dal.Context;
using Dal.Entities;
using Dal.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Dal.Repositories
{
    /// <summary>
    /// Work expperience repository
    /// </summary>
    public class WorkExperienceRepository:Repository<WorkExperience,ApplicationContext>, IWorkExperienceRepository
    {
        public WorkExperienceRepository(ApplicationContext context) : base(context) { }

        public IQueryable<WorkExperience> GetAllWithDetails()
        {
            return _context.WorkExperiences.Include(i => i.Summaries);
        }

        public async Task<WorkExperience> GetByIdWithDetailsAsync(int id)
        {
            return await _context.WorkExperiences.Include(i => i.Summaries).FirstOrDefaultAsync(i=>i.Id==id);
        }
    }
}

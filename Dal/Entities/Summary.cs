﻿using System;
using System.Collections.Generic;

namespace Dal.Entities
{
    public class Summary:BaseEntity
    {
        public int UserId { get; set; }
        public int PositionId { get; set; }
        public Position Position { get; set; }
        public ICollection<EmployersInSummaries> EmployersId { get; set; }
        public ICollection<WorkExperience> WorkExperiences { get; set; }
        public ICollection<Education> Educations { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public bool IsHidden { get; set; }
    }
}

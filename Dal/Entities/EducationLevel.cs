﻿namespace Dal.Entities
{
    public class EducationLevel:BaseEntity
    {
        public string Name { get; set; }
    }
}

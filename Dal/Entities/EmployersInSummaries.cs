﻿namespace Dal.Entities
{
    public class EmployersInSummaries:BaseEntity
    {
        public int SummaryId { get; set; }
        public int EmployerId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Dal.Entities
{
    public class WorkExperience:BaseEntity
    {
        public int UserId { get; set; }
        public string CompanyName { get; set; }
        public string Position { get; set; }
        public ICollection<Summary> Summaries { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? FinishDate { get; set; }
    }
}

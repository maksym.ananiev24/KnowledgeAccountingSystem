﻿using System.Collections.Generic;

namespace Dal.Entities
{
    public class Education : BaseEntity
    {
        public int UserId { get; set; }
        public int EducationLevelId { get; set; }
        public EducationLevel EducationLevel { get; set; }
        public ICollection<Summary> Summaries { get; set; }
        public string EducationalInstitution { get; set; }
        public string Department { get; set; }
        public int YearStart { get; set; }
        public int YearEnd { get; set; }
    }
}

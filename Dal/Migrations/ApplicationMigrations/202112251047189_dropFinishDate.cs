﻿namespace Dal.Migrations.ApplicationMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dropFinishDate : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.WorkExperiences", "FinishDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.WorkExperiences", "FinishDate", c => c.DateTime());
        }
    }
}

﻿namespace Dal.Migrations.ApplicationMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditSummaryCreationDateNotNull : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Summaries", "CreationDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Summaries", "CreationDate", c => c.DateTime());
        }
    }
}

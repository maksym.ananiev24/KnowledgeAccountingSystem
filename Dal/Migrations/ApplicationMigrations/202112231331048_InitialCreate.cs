﻿namespace Dal.Migrations.ApplicationMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EducationLevels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Educations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        EducationLevelId = c.Int(nullable: false),
                        EducationalInstitution = c.String(),
                        Department = c.String(),
                        YearStart = c.Int(nullable: false),
                        YearEnd = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EducationLevels", t => t.EducationLevelId, cascadeDelete: true)
                .Index(t => t.EducationLevelId);
            
            CreateTable(
                "dbo.Summaries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        PositionId = c.Int(nullable: false),
                        Description = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        DeletionDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Positions", t => t.PositionId, cascadeDelete: true)
                .Index(t => t.PositionId);
            
            CreateTable(
                "dbo.Positions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkExperiences",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        CompanyName = c.String(),
                        Position = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        FinishDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SummaryEducations",
                c => new
                    {
                        Summary_Id = c.Int(nullable: false),
                        Education_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Summary_Id, t.Education_Id })
                .ForeignKey("dbo.Summaries", t => t.Summary_Id, cascadeDelete: true)
                .ForeignKey("dbo.Educations", t => t.Education_Id, cascadeDelete: true)
                .Index(t => t.Summary_Id)
                .Index(t => t.Education_Id);
            
            CreateTable(
                "dbo.WorkExperienceSummaries",
                c => new
                    {
                        WorkExperience_Id = c.Int(nullable: false),
                        Summary_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.WorkExperience_Id, t.Summary_Id })
                .ForeignKey("dbo.WorkExperiences", t => t.WorkExperience_Id, cascadeDelete: true)
                .ForeignKey("dbo.Summaries", t => t.Summary_Id, cascadeDelete: true)
                .Index(t => t.WorkExperience_Id)
                .Index(t => t.Summary_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkExperienceSummaries", "Summary_Id", "dbo.Summaries");
            DropForeignKey("dbo.WorkExperienceSummaries", "WorkExperience_Id", "dbo.WorkExperiences");
            DropForeignKey("dbo.Summaries", "PositionId", "dbo.Positions");
            DropForeignKey("dbo.SummaryEducations", "Education_Id", "dbo.Educations");
            DropForeignKey("dbo.SummaryEducations", "Summary_Id", "dbo.Summaries");
            DropForeignKey("dbo.Educations", "EducationLevelId", "dbo.EducationLevels");
            DropIndex("dbo.WorkExperienceSummaries", new[] { "Summary_Id" });
            DropIndex("dbo.WorkExperienceSummaries", new[] { "WorkExperience_Id" });
            DropIndex("dbo.SummaryEducations", new[] { "Education_Id" });
            DropIndex("dbo.SummaryEducations", new[] { "Summary_Id" });
            DropIndex("dbo.Summaries", new[] { "PositionId" });
            DropIndex("dbo.Educations", new[] { "EducationLevelId" });
            DropTable("dbo.WorkExperienceSummaries");
            DropTable("dbo.SummaryEducations");
            DropTable("dbo.WorkExperiences");
            DropTable("dbo.Positions");
            DropTable("dbo.Summaries");
            DropTable("dbo.Educations");
            DropTable("dbo.EducationLevels");
        }
    }
}

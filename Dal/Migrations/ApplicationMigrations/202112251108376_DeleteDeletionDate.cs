﻿namespace Dal.Migrations.ApplicationMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteDeletionDate : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Summaries", "DeletionDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Summaries", "DeletionDate", c => c.DateTime());
        }
    }
}

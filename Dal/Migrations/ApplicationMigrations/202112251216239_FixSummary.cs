﻿namespace Dal.Migrations.ApplicationMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixSummary : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmployersInSummaries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SummaryId = c.Int(nullable: false),
                        EmployerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Summaries", t => t.SummaryId, cascadeDelete: true)
                .Index(t => t.SummaryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EmployersInSummaries", "SummaryId", "dbo.Summaries");
            DropIndex("dbo.EmployersInSummaries", new[] { "SummaryId" });
            DropTable("dbo.EmployersInSummaries");
        }
    }
}

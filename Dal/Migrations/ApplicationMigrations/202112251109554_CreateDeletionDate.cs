﻿namespace Dal.Migrations.ApplicationMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateDeletionDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Summaries", "DeletionDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Summaries", "DeletionDate");
        }
    }
}

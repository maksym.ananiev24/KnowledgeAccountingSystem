﻿namespace Dal.Migrations.ApplicationMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatFinishDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkExperiences", "FinishDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.WorkExperiences", "FinishDate");
        }
    }
}

﻿namespace Dal.Migrations.ApplicationMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditSummary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Summaries", "IsHidden", c => c.Boolean(nullable: false));
            DropColumn("dbo.Summaries", "DeletionDate");
            DropColumn("dbo.Summaries", "IsDeleted");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Summaries", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Summaries", "DeletionDate", c => c.DateTime());
            DropColumn("dbo.Summaries", "IsHidden");
        }
    }
}

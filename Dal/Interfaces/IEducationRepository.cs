﻿using Dal.Entities;

namespace Dal.Interfaces
{
    /// <summary>
    /// Repository interface for education model
    /// </summary>
    public interface IEducationRepository:IRepository<Education>, IRepositoryWithDetails<Education>
    {
    }
}

﻿using Dal.Entities;

namespace Dal.Interfaces
{
    /// <summary>
    /// Repository interface for role model
    /// </summary>
    public interface IRoleRepository:IRepository<Role>, IRepositoryWithDetails<Role>
    {
    }
}

﻿using Dal.Entities;

namespace Dal.Interfaces
{
    /// <summary>
    /// Repository interface for intermediate table
    /// </summary>
    public interface IEmployersInSummariesRepository:IRepository<EmployersInSummaries>
    {
    }
}

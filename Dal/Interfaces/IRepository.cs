﻿using Dal.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Dal.Interfaces
{
    /// <summary>
    /// Generic repository interface
    /// </summary>
    /// <typeparam name="TEntity">base entity model</typeparam>
    public interface IRepository<TEntity> where TEntity:BaseEntity
    {
        IQueryable<TEntity> FindAll();

        Task<TEntity> GetByIdAsync(int id);

        TEntity Add(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);

        Task DeleteByIdAsync(int id);
    }
}

﻿using Dal.Entities;
using System.Threading.Tasks;

namespace Dal.Interfaces
{
    /// <summary>
    /// Repository interface for user model
    /// </summary>
    public interface IUserRepository : IRepository<User>, IRepositoryWithDetails<User>
    {
        Task<User> GetUserByEmail(string email);
    }
}

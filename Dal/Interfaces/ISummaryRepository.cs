﻿using Dal.Entities;

namespace Dal.Interfaces
{
    /// <summary>
    /// Repository interface for summary model
    /// </summary>
    public interface ISummaryRepository:IRepository<Summary>, IRepositoryWithDetails<Summary>
    {
    }
}

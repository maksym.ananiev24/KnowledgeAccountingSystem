﻿using Dal.Entities;

namespace Dal.Interfaces
{
    /// <summary>
    /// Repository interface for workExperience model
    /// </summary>
    public interface IWorkExperienceRepository:IRepository<WorkExperience>, IRepositoryWithDetails<WorkExperience>
    {
    }
}

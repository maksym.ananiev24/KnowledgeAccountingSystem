﻿using Dal.Entities;

namespace Dal.Interfaces
{
    /// <summary>
    /// Repository interface for position model
    /// </summary>
    public interface IPositionRepository:IRepository<Position>
    {
    }
}

﻿using Dal.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Dal.Interfaces
{
    /// <summary>
    /// Extrention for generic repository
    /// </summary>
    /// <typeparam name="TEntity">base entity model</typeparam>
    public interface IRepositoryWithDetails<TEntity> where TEntity:BaseEntity
    {
        IQueryable<TEntity> GetAllWithDetails();
        Task<TEntity> GetByIdWithDetailsAsync(int id);
    }
}

﻿using Dal.Entities;

namespace Dal.Interfaces
{
    /// <summary>
    /// Repository interface for educationLevel model
    /// </summary>
    public interface IEducationLevelRepository:IRepository<EducationLevel>
    {

    }
}

﻿using Dal.Interfaces;
using System.Threading.Tasks;

namespace Dal.Uow
{
    /// <summary>
    /// Uow interface for repositories
    /// </summary>
    public interface IUnitOfWork
    {
        ISummaryRepository SummaryRepository { get; }
        IEducationRepository EducationRepository { get; }
        IEducationLevelRepository EducationLevelRepository { get; }
        IPositionRepository PositionRepository { get; }
        IWorkExperienceRepository WorkExperienceRepository { get; }
        IUserRepository UserRepository { get; }
        IRoleRepository RoleRepository { get; }
        IEmployersInSummariesRepository EmployersInSummariesRepository { get; }
        Task<int> SaveAsync();
    }
}

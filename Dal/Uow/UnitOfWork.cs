﻿using Dal.Context;
using Dal.Interfaces;
using Dal.Repositories;
using System;
using System.Threading.Tasks;

namespace Dal.Uow
{
    /// <summary>
    /// Uow implementation for repositories
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _applicationContext;

        private readonly UserContext _userContext;
        private IUserRepository _userRepository;
        public IUserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository(_userContext);
                }
                return _userRepository;
            }
        }

        private IRoleRepository _roleRepository;
        public IRoleRepository RoleRepository
        {
            get
            {
                if (_roleRepository == null)
                {
                    _roleRepository = new RoleRepository(_userContext);
                }
                return _roleRepository;
            }
        }

        private ISummaryRepository _summaryRepository;
        public ISummaryRepository SummaryRepository
        {
            get
            {
                if(_summaryRepository==null)
                {
                    _summaryRepository = new SummaryRepository(_applicationContext);
                }
                return _summaryRepository;
            }  
        }

        private IEducationRepository _educationRepository;
        public IEducationRepository EducationRepository
        {
            get
            {
                if (_educationRepository == null)
                {
                    _educationRepository = new EducationRepository(_applicationContext);
                }
                return _educationRepository;
            }
        }

        private IEducationLevelRepository _educationLevelRepository;
        public IEducationLevelRepository EducationLevelRepository
        {
            get
            {
                if (_educationLevelRepository == null)
                {
                    _educationLevelRepository = new EducationLevelRepository(_applicationContext);
                }
                return _educationLevelRepository;
            }
        }

        private IPositionRepository _positionRepository;
        public IPositionRepository PositionRepository
        {
            get
            {
                if (_positionRepository == null)
                {
                    _positionRepository = new PositionRepository(_applicationContext);
                }
                return _positionRepository;
            }
        }

        private IWorkExperienceRepository _workExperienceRepository;
        public IWorkExperienceRepository WorkExperienceRepository
        {
            get
            {
                if (_workExperienceRepository == null)
                {
                    _workExperienceRepository = new WorkExperienceRepository(_applicationContext);
                }
                return _workExperienceRepository;
            }
        }

        private IEmployersInSummariesRepository _employersInSummariesRepository;
        public IEmployersInSummariesRepository EmployersInSummariesRepository 
        {
            get
            {
                if (_employersInSummariesRepository == null)
                {
                    _employersInSummariesRepository = new EmployersInSummariesRepository(_applicationContext);
                }
                return _employersInSummariesRepository;
            }
        }

        public UnitOfWork(ApplicationContext applicationContext, UserContext userContext)
        {
            _applicationContext = applicationContext;
            _userContext = userContext;
        }

        public async Task<int> SaveAsync()
        {
            int sum = 0;
            sum = await _userContext.SaveChangesAsync();
            sum += await _applicationContext.SaveChangesAsync();
            return sum;
        }
    }
}

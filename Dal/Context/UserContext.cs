﻿using Dal.Entities;
using System.Data.Entity;

namespace Dal.Context
{
    /// <summary>
    /// User data base context class
    /// </summary>
    public class UserContext:DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

        public UserContext() : base("UserConnectionString") { }
        public UserContext(string connection) : base(connection) { }
        static UserContext()
        {
            Database.SetInitializer(new UserDbInitializer());
        }
    }

    public class UserDbInitializer : CreateDatabaseIfNotExists<UserContext>
    {
        protected override void Seed(UserContext db)
        {
            db.Roles.Add(new Role { Id = 1, Name = "Admin" });
            db.Roles.Add(new Role { Id = 2, Name = "User" });
            db.Roles.Add(new Role { Id = 3, Name = "Employer" });
            db.Users.Add(new User
            {
                Name = "Admin",
                Email = "admin@example.com",
                Password = "f019e6438385dcb9baba6f6f721d2b52",//Admin123
                RoleId = 1
            });
            db.Users.Add(new User
            {
                Name = "Maksym",
                Surname="Ananiev",
                Age=20,
                Email = "maksim@gmail.com",
                PhoneNumber="381234567890",
                Password = "6bb476703104a2a826be400f2b2b78db",//Maksim123
                RoleId = 2
            });
            db.Users.Add(new User
            {
                Name = "Employer",
                Email = "employer@example.com",
                Password = "bb717d3bb4106bfaca145752ff1a38a9",//Employer123
                RoleId = 3
            });
            db.Users.Add(new User
            {
                Name = "Emil",
                Surname= "Gadzhyiev",
                Age = 22,
                Email = "Emil@gmail.com",
                PhoneNumber = "380987654321",
                Password = "dd497e436721d16bf199acd0043eae1a",//Emil123
                RoleId = 2
            });
            db.SaveChanges();
        }
    }
}

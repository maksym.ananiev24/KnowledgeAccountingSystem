﻿using Dal.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Dal.Context
{
    /// <summary>
    /// Application data base context class
    /// </summary>
    public class ApplicationContext:DbContext
    {
        public DbSet<Summary> Summaries { get; set; }
        public DbSet<Education> Educations { get; set; }
        public DbSet<EducationLevel> EducationLevels { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<WorkExperience> WorkExperiences { get; set; }
        public DbSet<EmployersInSummaries> EmployersInSummaries { get; set; }

        public ApplicationContext():base("AppConnectionString") { }
        public ApplicationContext(string connection) : base(connection) { }
        static ApplicationContext()
        {
            Database.SetInitializer(new ApplicationDbInitializer());
        }
    }

    public class ApplicationDbInitializer : CreateDatabaseIfNotExists<ApplicationContext>
    {
        protected override void Seed(ApplicationContext db)
        {
            var positions = new List<Position>()
            {
                new Position()
                {
                    Name=".Net developer"
                },
                new Position()
                {
                    Name="Java developer"
                },
                new Position()
                {
                    Name="Python developer"
                },
                new Position()
                {
                    Name="JavaScript developer"
                },
                new Position()
                {
                    Name="Php developer"
                },
            };
            db.Positions.AddRange(positions);
            db.SaveChanges();

            var educationLevels = new List<EducationLevel>()
            {
                new EducationLevel()
                {
                    Name="Higher"
                },
                new EducationLevel()
                {
                    Name="Incomplete higher"
                },
                new EducationLevel()
                {
                    Name="Secondary special"
                },
                new EducationLevel()
                {
                    Name="Secondary"
                }
            };
            db.EducationLevels.AddRange(educationLevels);
            db.SaveChanges();

            var educations = new List<Education>()
            {
                new Education()
                {
                    Id=1,
                    EducationLevelId=2,
                    UserId=2,
                    Department="Computer science",
                    EducationalInstitution="Kharkiv National University of Radio Electronics",
                    YearStart=2018,
                    YearEnd=2022
                },
                new Education()
                {
                    Id=2,
                    EducationLevelId=4,
                    UserId=2,
                    Department="Regular school curriculum",
                    EducationalInstitution="Kharkov specialized school",
                    YearStart=2007,
                    YearEnd=2018
                },
                new Education()
                {
                    Id=3,
                    EducationLevelId=1,
                    UserId=4,
                    Department="Computer science",
                    EducationalInstitution="VN Karazin Kharkiv National University",
                    YearStart=2016,
                    YearEnd=2020
                },
                new Education()
                {
                    Id=4,
                    EducationLevelId=2,
                    UserId=2,
                    Department="Regular school curriculum",
                    EducationalInstitution="Kharkiv comprehensive school of I-III degrees №88",
                    YearStart=1999,
                    YearEnd=2020
                },
            };
            db.Educations.AddRange(educations);
            db.SaveChanges();

            var workExperiences = new List<WorkExperience>()
            {
                new WorkExperience()
                {
                    Id=1,
                    CompanyName="Epam =)",
                    Position="Junior .Net Developer",
                    UserId=2,
                    StartDate=DateTime.Now.AddDays(-200)
                },
                new WorkExperience()
                {
                    Id=2,
                    CompanyName="LLC \"LD Kharkiv\"",
                    Position="Assistant accountant",
                    UserId=4,
                    StartDate=DateTime.Now.AddYears(-2),
                    FinishDate=DateTime.Now.AddYears(-1),
                },
                new WorkExperience()
                {
                    Id=3,
                    CompanyName="Easy3Dprint",
                    Position="Technician / repairman of 3D printers",
                    UserId=4,
                    StartDate=DateTime.Now.AddDays(-150)
                }
            };

            db.WorkExperiences.AddRange(workExperiences);
            db.SaveChanges();

            var summaries = new List<Summary>()
            {
                new Summary()
                {
                    UserId=2,
                    WorkExperiences=new List<WorkExperience>(){workExperiences.First()},
                    CreationDate=DateTime.Now.AddDays(-2),
                    Description="My new test summary",
                    Educations=new List<Education>(){educations.First()},
                    PositionId=1
                },
                new Summary()
                {
                    UserId=4,
                    WorkExperiences=new List<WorkExperience>(){workExperiences.ElementAt(1)},
                    CreationDate=DateTime.Now.AddDays(-20),
                    Description="3 week intensive on Python, Web base 1 completed training project in the team The rest of the time he worked with unit tests and functionality on an internal project before quarantine Technologies used: Python, Django REST, Docker, Celery, PostgeSQL, (GoogleDriveAPI, Youtube API, social - auth), Unittests",
                    Educations=new List<Education>(){educations.ElementAt(2),educations.ElementAt(3)},
                    PositionId=3
                },
                new Summary()
                {
                    UserId=4,
                    WorkExperiences=new List<WorkExperience>(){workExperiences.ElementAt(1),workExperiences.ElementAt(2)},
                    CreationDate=DateTime.Now.AddDays(-60),
                    Description="I am looking for a long-term job where I can study and benefit the company. Python (back-end / fullstack) Fulltime / remotely is not important. I have experience of working remotely. Resistant to stress. I checked this when I was working on the election commission.",
                    Educations=new List<Education>(){educations.ElementAt(2)},
                    PositionId=1
                },
            };
            db.Summaries.AddRange(summaries);
            db.SaveChanges();
        }
    }
}

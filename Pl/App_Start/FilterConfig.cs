﻿using Pl.Helper.Filters;
using System.Web.Mvc;

namespace Pl
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ArgumentExceptionFilter());
            filters.Add(new LoggerExceptionFilter());
        }
    }
}

﻿using Serilog;
using System;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using System.Configuration;

namespace Pl.Helper.Filters
{
    public class LoggerExceptionFilter : IExceptionFilter
    {
        static LoggerExceptionFilter()
        {
            var localPath= ConfigurationManager.AppSettings["logPath"];
            var path = System.Web.Hosting.HostingEnvironment.MapPath(localPath);
            Log.Logger = new LoggerConfiguration().WriteTo.File(path).CreateLogger();
        }
        public void OnException(ExceptionContext filterContext)
        {
            var path = GetPath(filterContext.RouteData);
            var user = ((Controller)filterContext.Controller).User;
            Log.Logger.Information($"{DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss")} | User: {user} | Path: {path}");
        }

        private string GetPath(RouteData routeData)
        {
            var path = new StringBuilder(string.Empty);
            foreach (var item in routeData.Values)
            {
                path.Append($"/{item}");
            }
            return path.ToString();
        }
    }
}
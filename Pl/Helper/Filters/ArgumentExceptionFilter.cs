﻿using System;
using System.Web.Mvc;

namespace Pl.Helper.Filters
{
    public class ArgumentExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled && filterContext.Exception is ArgumentException)
            {
                filterContext.Result = new ViewResult()
                {
                    ViewName = "ArgumentExceprion",
                    ViewData = new ViewDataDictionary(filterContext.Exception.Message)
                };
                filterContext.ExceptionHandled = true;
            }
        }
    }
}
﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Pl.Helper.HtmlExtensions
{
    public static class StringSeparatorExtensions
    {
        public static IHtmlString SeparateString(this HtmlHelper helper, string text)
        {
            string[] stringSeparators = new string[] { "\r\n" };
            string[] paragraphs = text.Split(stringSeparators, StringSplitOptions.None);
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < paragraphs.Length; i++)
            {
                builder.Append($"&emsp;{paragraphs[i]}");
                if (i != paragraphs.Length - 1)
                {
                    builder.Append("<br>");
                }
            }
            return MvcHtmlString.Create(builder.ToString());
        }
    }
}
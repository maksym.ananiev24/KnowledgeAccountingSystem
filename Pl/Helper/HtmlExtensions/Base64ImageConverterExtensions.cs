﻿using System.Web;
using System.Web.Mvc;

namespace Pl.Helper.HtmlExtensions
{
    public static class Base64ImageConverterExtensions
    {
        public static IHtmlString ConvertBase64ToImage(this HtmlHelper helper, string image)
        {
            return MvcHtmlString.Create($"<img src = \"data:image/gif;base64,{image}\" class=\"img-thumbnail\">");
        }
    }
}
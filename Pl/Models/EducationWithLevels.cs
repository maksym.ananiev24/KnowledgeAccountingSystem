﻿using Bll.Models;
using System.Collections.Generic;

namespace Pl.Models
{
    public class EducationWithLevels
    {
        public EducationModel EducationModel { get; set; }
        public IEnumerable<EducationLevelModel> EducationLevelModels { get; set; }
    }
}
﻿using System.Collections.Generic;
using Bll.Models;

namespace Pl.Models
{
    public class UserWithSummaries
    {
        public UserModel UserModel;
        public IEnumerable<SummaryModel> SummaryModels;
    }
}
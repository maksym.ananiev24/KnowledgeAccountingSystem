﻿using Bll.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pl.Models
{
    public class FullSummary
    {
        [Required]
        public SummaryModel Summary { get; set; }
        public IEnumerable<PositionModel> Positions { get; set; }
        public IEnumerable<EducationModel> Educations { get; set; }
        public IEnumerable<WorkExperienceModel> WorkExperiences { get; set; }
        public IEnumerable<int> EducationIds { get; set; }
        public IEnumerable<int> WorkExperienceIds { get; set; }
    }
}
﻿using Bll.Extensions;
using Bll.Interfaces;
using Bll.Models;
using Pl.Models;
using PagedList;
using Rotativa;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Pl.Controllers
{
    /// <summary>
    /// Summary controller
    /// </summary>
    [Authorize]
    public class SummaryController : Controller
    {
        private readonly ISummaryService _summaryService;
        private readonly IPositionService _positionService;
        private readonly IUserService _userService;
        private readonly IEducationService _educationService;
        private readonly IWorkExperienceService _workExperienceService;
        public SummaryController(ISummaryService summaryService, IUserService userService, IPositionService positionService,
            IEducationService educationService, IWorkExperienceService workExperienceService)
        {
            _summaryService = summaryService;
            _userService = userService;
            _positionService = positionService;
            _educationService = educationService;
            _workExperienceService = workExperienceService;
        }

        [AllowAnonymous]
        public async Task<ActionResult> Main(int page=1, IEnumerable<int> positionIds=null)
        {
            int pageSize = 4;
            var userWithSummaries = await _summaryService.GetAvailableSummariesByPositionIdAsync(positionIds);
            ViewBag.Positions = await _positionService.GetAllAsync();
            ViewBag.PositionIds = positionIds;
            return View(userWithSummaries.ToPagedList(page, pageSize));
        }

        public async Task<ActionResult> PrintSummaryToPdf(int id)
        {
            var summary = await _summaryService.GetByIdAsync(id);
            var userWithSummary = new UserWithSummary()
            {
                UserModel = await _userService.GetUserByIdAsync(summary.UserId),
                SummaryModel = summary
            };
            return new ViewAsPdf("SummaryToPrint", userWithSummary);
        }

        [Authorize(Roles = "User")]
        public async Task<ActionResult> ShowSummariesByEducationId(int id)
        {
            return PartialView("SummariesListPartial", await _summaryService.GetAllByEducationIdAsync(User.Identity.GetUserId(), id));
        }

        [Authorize(Roles = "User")]
        public async Task<ActionResult> ShowSummariesByWorkExperienceId(int id)
        {
            return PartialView("SummariesListPartial", await _summaryService.GetAllByWorkIdAsync(User.Identity.GetUserId(), id));
        }

        [Authorize(Roles = "User")]
        public async Task<ActionResult> ShowUserSummaries()
        {
            return View("AllSummaries", await _summaryService.GetAllByUserIdAsync(User.Identity.GetUserId()));
        }

        [Authorize(Roles = "User")]
        public ActionResult CreateNewSummary()
        {
            return RedirectToAction("ChangeProfileAjax", "User");
        }

        [Authorize(Roles = "User")]
        public async Task<ActionResult> AddSummaryAjax()
        {
            var userId = User.Identity.GetUserId();
            var fullSummary = new FullSummary()
            {
                Positions = await _positionService.GetAllAsync(),
                Educations=await _educationService.GetAllByUserIdAsync(userId),
                WorkExperiences=await _workExperienceService.GetAllByUserIdAsync(userId),
                Summary=new SummaryModel()
            };
            return PartialView("AddSummaryPartialAjax", fullSummary);
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        public async Task<ActionResult> AddSummary(FullSummary fullSummary)
        {
            if (!ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                fullSummary.Positions = await _positionService.GetAllAsync();
                fullSummary.Educations = await _educationService.GetAllByUserIdAsync(userId);
                fullSummary.WorkExperiences = await _workExperienceService.GetAllByUserIdAsync(userId);
                return PartialView("AddSummaryPartialAjax", fullSummary);
            }
            else
            {
                fullSummary.Summary.UserId = User.Identity.GetUserId();
                await _summaryService.CreateFullSummaryAsync(fullSummary.Summary, fullSummary.EducationIds, fullSummary.WorkExperienceIds);
                return PartialView("AddSuccess");
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> GetSummaryDetails(int id)
        {
            var summary = await _summaryService.GetByIdAsync(id);
            var userWithSummary = new UserWithSummary()
            {
                UserModel = await _userService.GetUserByIdAsync(summary.UserId),
                SummaryModel = summary
            };
            return View("SummaryDetails", userWithSummary);
        }

        [Authorize(Roles = "User")]
        public async Task<ActionResult> RemoveSummary(int id)
        {
            await _summaryService.DeleteByIdAsync(id);
            return PartialView("SummariesListPartial", await _summaryService.GetAllByUserIdAsync(User.Identity.GetUserId()));
        }

        [Authorize(Roles = "User")]
        public async Task<ActionResult> RestoreSummary(int id)
        {
            await _summaryService.RestoreSummaryAsync(id);
            return PartialView("SummariesListPartial", await _summaryService.GetAllByUserIdAsync(User.Identity.GetUserId()));
        }

        [Authorize(Roles = "User")]
        public async Task<ActionResult> HideSummary(int id)
        {
            await _summaryService.HideSummaryAsync(id);
            return PartialView("SummariesListPartial", await _summaryService.GetAllByUserIdAsync(User.Identity.GetUserId()));
        }

        [Authorize(Roles = "User")]
        public async Task<ActionResult> EditSummary(int id)
        {
            var userId = User.Identity.GetUserId();
            var summary = await _summaryService.GetByIdAsync(id);
            var fullSummary = new FullSummary()
            {
                Summary = summary,
                Positions = await _positionService.GetAllAsync(),
                Educations = await _educationService.GetAllByUserIdAsync(userId),
                WorkExperiences = await _workExperienceService.GetAllByUserIdAsync(userId),
                EducationIds= summary.Educations.Select(i=>i.Id),
                WorkExperienceIds = summary.WorkExperiences.Select(i => i.Id)
            };
            return View(fullSummary);
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        public async Task<ActionResult> EditSummary(FullSummary fullSummary)
        {
            if(ModelState.IsValid)
            {
                await _summaryService.UpdateFullSummaryAsync(fullSummary.Summary, fullSummary.EducationIds, fullSummary.WorkExperienceIds);
                return RedirectToAction("ShowUserSummaries");
            }
            else
            {
                var userId = User.Identity.GetUserId();
                fullSummary.Positions = await _positionService.GetAllAsync();
                fullSummary.Educations = await _educationService.GetAllByUserIdAsync(userId);
                fullSummary.WorkExperiences = await _workExperienceService.GetAllByUserIdAsync(userId);
                return View(fullSummary);
            }
        }

        [Authorize(Roles = "Employer")]
        public async Task<ActionResult> GetUserFavorites()
        {
            return View("Favorites", await _summaryService.GetAvailableSummariesFromFavorites(User.Identity.GetUserId()));
        }

        [Authorize(Roles = "Employer")]
        public async Task<ActionResult> AddToFavorites(int id)
        {
            await _summaryService.AddToFavorites(id, User.Identity.GetUserId());
            return RedirectToAction("GetSummaryDetails", new { id });
        }

        [Authorize(Roles = "Employer")]
        public async Task<ActionResult> RemoveFromFavorites(int id)
        {
            await _summaryService.RemoveFromFavorites(id, User.Identity.GetUserId());
            return RedirectToAction("GetSummaryDetails", new { id });
        }
    }
}
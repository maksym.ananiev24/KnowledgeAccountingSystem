﻿using Bll.Interfaces;
using Bll.Models;
using Bll.Extensions;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Pl.Models;
using Bll;

namespace Pl.Controllers
{
    /// <summary>
    /// User controller
    /// </summary>
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;
        private readonly ISummaryService _summaryService;
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public UserController(IUserService userService, IRoleService roleService, ISummaryService summaryService)
        {
            _userService = userService;
            _roleService = roleService;
            _summaryService = summaryService;
        }

        public async Task<ActionResult> Register()
        {
            ViewBag.Roles = await _roleService.GetRolesForRegistrationAsync();
            return View(new RegisterModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    await _userService.RegisterAsync(model);
                    return RedirectToAction("Login");
                }
                catch(CustomException ex)
                {
                    ModelState.AddModelError(ex.ParamName, ex.Message);
                }
            }
            ViewBag.Roles = await _roleService.GetRolesForRegistrationAsync();
            return View(model);
        }

        public ActionResult Login()
        {
            return View(new LoginModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel login)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    var claims = await _userService.AuthenticateAsync(login);
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claims);
                    return RedirectToAction("Main", "Summary");
                }
                catch (CustomException ex)
                {
                    ModelState.AddModelError(ex.ParamName, ex.Message);
                }
            }
            return View(login);
        }

        [Authorize]
        public async Task<ActionResult> GetProfile()
        {
            var userId = User.Identity.GetUserId();
            var userWithSummaries = new UserWithSummaries()
            {
                UserModel = await _userService.GetUserByIdAsync(userId),
                SummaryModels = await _summaryService.GetAllByUserIdAsync(userId)
            };
            return View("Profile", userWithSummaries);
        }

        [Authorize]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Main", "Summary");
        }

        [Authorize]
        public async Task<ActionResult> ChangeProfile()
        {
            return View(await _userService.GetUserByIdAsync(User.Identity.GetUserId()));
        }

        [Authorize]
        [Authorize(Roles = "User")]
        public async Task<ActionResult> ChangeProfileAjax()
        {
            return View("CreateSummary", await _userService.GetUserByIdAsync(User.Identity.GetUserId()));
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        public async Task<ActionResult> ChangeProfileAjax(UserModel model)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    await ChangeUserProfile(model);
                    return RedirectToAction("AddSummaryAjax", "Summary");
                }
                catch (CustomException ex)
                {
                    ModelState.AddModelError(ex.ParamName, ex.Message);
                }
            }
            return PartialView(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> ChangeProfile(UserModel model)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    await ChangeUserProfile(model);
                    return RedirectToAction("GetProfile");
                }
                catch(CustomException ex)
                {
                    ModelState.AddModelError(ex.ParamName, ex.Message);
                }
            }
            return View(model);
        }

        private async Task ChangeUserProfile(UserModel model)
        {
            model.Id = User.Identity.GetUserId();
            model.RoleId = User.Identity.GetUserRole();
            await _userService.UpdateUserAsync(model);
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> AllUsers()
        {
            ViewBag.Roles = await _roleService.GetAllRolesAsync();
            return View(await _userService.GetAllUsersAsync());
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> ChangeUserRole(int userId, int userRole)
        {
            await _userService.ChangeUserRoleAsync(userId, userRole);
            return RedirectToAction("AllUsers");
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> DeleteUser(int userId)
        {
            await _userService.DeleteUserAsync(userId);
            await _summaryService.RemoveAllByUserIdAsync(userId);
            return RedirectToAction("AllUsers");
        }
    }
}
﻿using System.Web.Mvc;

namespace Pl.Controllers
{
    /// <summary>
    /// Error controller
    /// </summary>
    public class ErrorController : Controller
    {
        public ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View("Ex404");
        }
    }
}
﻿using Bll;
using Bll.Extensions;
using Bll.Interfaces;
using Bll.Models;
using Pl.Models;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Pl.Controllers
{
    /// <summary>
    /// Education controller
    /// </summary>
    [Authorize(Roles = "User")]
    public class EducationController : Controller
    {
        private readonly IEducationService _educationService;
        private readonly IEducationLevelService _educationLevelService;
        public EducationController(IEducationService educationService, IEducationLevelService educationLevelService)
        {
            _educationService = educationService;
            _educationLevelService = educationLevelService;
        }

        public async Task<ActionResult> GetAllEducations()
        {
            return PartialView("AllEducations", await _educationService.GetAllByUserIdAsync(User.Identity.GetUserId()));
        }

        public async Task<ActionResult> RemoveEducation(int id)
        {
            await _educationService.DeleteByIdAsync(id);
            return PartialView("AllEducationsPartial", await _educationService.GetAllByUserIdAsync(User.Identity.GetUserId()));
        }

        public async Task<ActionResult> AddEducation()
        {
            var educationWithLevels = new EducationWithLevels()
            {
                EducationLevelModels = await _educationLevelService.GetAllAsync(),
                EducationModel = new EducationModel()
            };
            return View(educationWithLevels);
        }

        [HttpPost]
        public async Task<ActionResult> AddEducation(EducationWithLevels educationWithLevels)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    educationWithLevels.EducationModel.UserId = User.Identity.GetUserId();
                    await _educationService.AddAsync(educationWithLevels.EducationModel);
                    return RedirectToAction("GetAllEducations");
                }
                catch(CustomException ex)
                {
                    ModelState.AddModelError(ex.ParamName, ex.Message);
                }
            }
            educationWithLevels.EducationLevelModels = await _educationLevelService.GetAllAsync();
            return View(educationWithLevels);
        }

        public async Task<ActionResult> EditEducation(int id)
        {
            var educationWithLevels = new EducationWithLevels()
            {
                EducationModel = await _educationService.GetByIdAsync(id),
                EducationLevelModels = await _educationLevelService.GetAllAsync()
            };
            return View(educationWithLevels);
        }

        [HttpPost]
        public async Task<ActionResult> EditEducation(EducationWithLevels educationWithLevels)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _educationService.UpdateAsync(educationWithLevels.EducationModel);
                    return RedirectToAction("GetAllEducations");
                }
                catch (CustomException ex)
                {
                    ModelState.AddModelError(ex.ParamName, ex.Message);
                }
            }
            educationWithLevels.EducationLevelModels = await _educationLevelService.GetAllAsync();
            return View(educationWithLevels);
        }
    }
}
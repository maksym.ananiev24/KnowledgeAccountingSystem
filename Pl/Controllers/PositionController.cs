﻿using Bll;
using Bll.Interfaces;
using Bll.Models;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Pl.Controllers
{
    /// <summary>
    /// Position controller
    /// </summary>
    [Authorize(Roles ="Admin")]
    public class PositionController : Controller
    {
        private readonly IPositionService _positionService;
        public PositionController(IPositionService positionService)
        {
            _positionService = positionService;
        }
        public ActionResult CreatePosition()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreatePosition(PositionModel position)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    await _positionService.AddAsync(position);
                    return RedirectToAction("Main", "Summary");
                }
                catch(CustomException ex)
                {
                    ModelState.AddModelError(ex.ParamName, ex.Message);
                }
            }
            return View(position);
        }
    }
}
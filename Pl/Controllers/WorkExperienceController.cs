﻿using Bll;
using Bll.Extensions;
using Bll.Interfaces;
using Bll.Models;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Pl.Controllers
{
    /// <summary>
    /// Work Experience controller
    /// </summary>
    [Authorize(Roles = "User")]
    public class WorkExperienceController : Controller
    {
        private readonly IWorkExperienceService _workExperienceService;
        public WorkExperienceController(IWorkExperienceService workExperienceService)
        {
            _workExperienceService = workExperienceService;
        }
        public async Task<ActionResult> GetAllWorkExperiences()
        {
            return PartialView("AllWorkExperiences", await _workExperienceService.GetAllByUserIdAsync(User.Identity.GetUserId()));
        }

        public ActionResult AddWorkExperience()
        {
            return View(new WorkExperienceModel());
        }

        public async Task<ActionResult> RemoveWorkExperience(int id)
        {
            await _workExperienceService.DeleteByIdAsync(id);
            return PartialView("AllWorkExperiencesPartial", await _workExperienceService.GetAllByUserIdAsync(User.Identity.GetUserId()));
        }

        [HttpPost]
        public async Task<ActionResult> AddWorkExperience(WorkExperienceModel workExperienceModel)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    workExperienceModel.UserId = User.Identity.GetUserId();
                    await _workExperienceService.AddAsync(workExperienceModel);
                    return RedirectToAction("GetAllWorkExperiences");
                }
                catch(CustomException ex)
                {
                    ModelState.AddModelError(ex.ParamName, ex.Message);
                }
            }
            return View(workExperienceModel);
        }

        public async Task<ActionResult> EditWorkExperience(int id)
        {
            return View(await _workExperienceService.GetByIdAsync(id));
        }

        [HttpPost]
        public async Task<ActionResult> EditWorkExperience(WorkExperienceModel workExperienceModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _workExperienceService.UpdateAsync(workExperienceModel);
                    return RedirectToAction("GetAllWorkExperiences");
                }
                catch (CustomException ex)
                {
                    ModelState.AddModelError(ex.ParamName, ex.Message);
                }
            }
            return View(workExperienceModel);
        }
    }
}
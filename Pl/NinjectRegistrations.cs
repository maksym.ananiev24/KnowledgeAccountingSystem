﻿using Bll.Interfaces;
using Bll.Services;
using Ninject.Modules;

namespace Pl
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserService>().To<UserService>();
            Bind<IRoleService>().To<RoleService>();
            Bind<ISummaryService>().To<SummaryService>();
            Bind<IWorkExperienceService>().To<WorkExperienceService>();
            Bind<IEducationService>().To<EducationService>();
            Bind<IEducationLevelService>().To<EducationLevelService>();
            Bind<IPositionService>().To<PositionService>();
        }
    }
}
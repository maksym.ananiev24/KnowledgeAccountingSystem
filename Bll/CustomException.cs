﻿using System;

namespace Bll
{
    public class CustomException:Exception
    {
        public string ParamName { get; }
        public CustomException() { }
        public CustomException(string paramName, string message) : base(message) 
        {
            ParamName = paramName;
        }
    }
}

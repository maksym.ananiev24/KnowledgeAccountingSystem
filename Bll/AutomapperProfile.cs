﻿using AutoMapper;
using Bll.Models;
using Dal.Entities;
using System.Linq;

namespace Bll
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<RegisterModel, User>()
                .ForMember(i1 => i1.Email, i2 => i2.MapFrom(i => i.Email))
                .ForMember(i1 => i1.Password, i2 => i2.MapFrom(i => i.Password));

            CreateMap<User, UserModel>()
                .ForMember(i1 => i1.Role, i2 => i2.MapFrom(i => i.Role.Name));

            CreateMap<UserModel, User>();

            CreateMap<Role, RoleModel>().ReverseMap();

            CreateMap<Summary, SummaryModel>()
                .ForMember(i1 => i1.Position, i2 => i2.MapFrom(i => i.Position.Name))
                .ForMember(i1 => i1.EmployersId, i2 => i2.MapFrom(i => i.EmployersId.Select(inner=>inner.EmployerId)));
            CreateMap<SummaryModel, Summary>()
                .ForMember(i1 => i1.EmployersId, i2 => i2.Ignore());

            CreateMap<Education, EducationModel>()
                .ForMember(i1 => i1.EducationLevel, i2 => i2.MapFrom(i => i.EducationLevel.Name));
            CreateMap<EducationModel, Education>();

            CreateMap<WorkExperienceModel, WorkExperience>()
                .ForMember(i1 => i1.Summaries, i2 => i2.Ignore());
            CreateMap<WorkExperience, WorkExperienceModel>();

            CreateMap<EducationLevel, EducationLevelModel>().ReverseMap();

            CreateMap<Position, PositionModel>().ReverseMap();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bll.Models
{
    public class SummaryModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int PositionId { get; set; }
        public string Position { get; set; }
        public IEnumerable<int> EmployersId { get; set; }
        public IEnumerable<WorkExperienceModel> WorkExperiences { get; set; }
        public IEnumerable<EducationModel> Educations { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [MinLength(10)]
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public bool IsHidden { get; set; }
    }
}

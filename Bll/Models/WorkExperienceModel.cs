﻿using Bll.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bll.Models
{
    public class WorkExperienceModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public ICollection<SummaryModel> Summaries { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [MinLength(5)]
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [MinLength(2)]
        public string Position { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [DateRange]
        [DisplayFormat(DataFormatString = "{0:MMMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }
        [DateRange]
        [DisplayFormat(DataFormatString = "{0:MMMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FinishDate { get; set; }
    }
}

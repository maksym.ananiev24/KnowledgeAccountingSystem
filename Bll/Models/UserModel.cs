﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace Bll.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        [Required]
        [MinLength(2)]
        [RegularExpression("[A-Z][a-z]*", ErrorMessage = "Use letters only please with first letter in upper case")]
        public string Name { get; set; }
        [Required]
        [MinLength(2)]
        [RegularExpression("[A-Z][a-z]*", ErrorMessage = "Use letters only please with first letter in upper case")]
        public string Surname { get; set; }
        [MinLength(2)]
        [RegularExpression("[A-Z][a-z]*", ErrorMessage = "Use letters only please with first letter in upper case")]
        public string Patronymic { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [RegularExpression("[0-9]{12}", ErrorMessage = "Enter 12 digits starting with the country code")]
        public string PhoneNumber { get; set; }
        public int RoleId { get; set; }
        public string Role { get; set; }
        [Required]
        [Range(14,120)]
        public int Age { get; set; }
        public string ProfileImage { get; set; }
        [NotMapped]
        public HttpPostedFileBase Image { get; set; }
    }
}

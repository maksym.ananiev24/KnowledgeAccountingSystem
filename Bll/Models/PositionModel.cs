﻿using System.ComponentModel.DataAnnotations;

namespace Bll.Models
{
    public class PositionModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [MinLength(2)]
        public string Name { get; set; }
    }
}

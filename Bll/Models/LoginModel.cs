﻿using System.ComponentModel.DataAnnotations;

namespace Bll.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Field is required")]
        [EmailAddress]
        public string Email { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [MinLength(5)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}

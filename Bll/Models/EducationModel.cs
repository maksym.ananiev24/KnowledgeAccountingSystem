﻿using Bll.Extensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bll.Models
{
    public class EducationModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int EducationLevelId { get; set; }
        public string EducationLevel { get; set; }
        public ICollection<SummaryModel> Summaries { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [MinLength(5)]
        public string EducationalInstitution { get; set; }
        [MinLength(5)]
        public string Department { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [RangeUntilCurrentYearPlus(1900,0)]
        public int YearStart { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [RangeUntilCurrentYearPlus(1900,10)]
        public int YearEnd { get; set; }
    }
}

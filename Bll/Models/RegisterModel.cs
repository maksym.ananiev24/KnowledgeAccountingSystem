﻿using System.ComponentModel.DataAnnotations;

namespace Bll.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Field is required")]
        [EmailAddress]
        public string Email { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [MinLength(5)]
        [RegularExpression("(?=.*?[0-9])(?=.*?[a-z])(?=.*?[A-Z]).+", ErrorMessage = "Required one letter in uppercase, one in lower and one digit")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [MinLength(5)]
        [RegularExpression("(?=.*?[0-9])(?=.*?[a-z])(?=.*?[A-Z]).+", ErrorMessage = "Required one letter in uppercase, one in lower and one digit")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int RoleId { get; set; }
    }
}

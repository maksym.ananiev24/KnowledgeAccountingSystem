﻿using Bll.Models;

namespace Bll.Interfaces
{
    /// <summary>
    /// Position logic interface
    /// </summary>
    public interface IPositionService:ICrud<PositionModel>
    {
    }
}

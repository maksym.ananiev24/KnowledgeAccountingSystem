﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bll.Interfaces
{
    /// <summary>
    /// Crud operations
    /// </summary>
    /// <typeparam name="TModel">model type</typeparam>
    public interface ICrud<TModel> where TModel : class
    {
        Task<IEnumerable<TModel>> GetAllAsync();

        Task<TModel> GetByIdAsync(int id);

        Task<TModel> AddAsync(TModel model);

        Task UpdateAsync(TModel model);

        Task DeleteByIdAsync(int modelId);
    }

}

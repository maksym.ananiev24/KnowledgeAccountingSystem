﻿using Bll.Models;

namespace Bll.Interfaces
{
    /// <summary>
    /// Eduucation level logic interface
    /// </summary>
    public interface IEducationLevelService:ICrud<EducationLevelModel>
    {
    }
}

﻿using Bll.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bll.Interfaces
{
    /// <summary>
    /// Work experience logic interface
    /// </summary>
    public interface IWorkExperienceService:ICrud<WorkExperienceModel>
    {
        Task<IEnumerable<WorkExperienceModel>> GetAllByUserIdAsync(int userId);
    }
}

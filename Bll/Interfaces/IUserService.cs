﻿using Bll.Models;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Bll.Interfaces
{
    /// <summary>
    /// User logic interface
    /// </summary>
    public interface IUserService
    {
        Task<UserModel> GetUserByIdAsync(int Id);
        Task<ClaimsIdentity> AuthenticateAsync(LoginModel model);
        Task RegisterAsync(RegisterModel model);
        Task<IEnumerable<UserModel>> GetAllUsersAsync();
        Task ChangeUserRoleAsync(int userId, int roleId);
        Task DeleteUserAsync(int userId);
        Task UpdateUserAsync(UserModel model);
    }
}

﻿using Bll.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bll.Interfaces
{
    /// <summary>
    /// Suummary logic interface
    /// </summary>
    public interface ISummaryService:ICrud<SummaryModel>
    {
        Task<IEnumerable<SummaryModel>> GetAllByUserIdAsync(int userId);
        Task<IEnumerable<UserWithSummary>> GetAvailableSummariesByPositionIdAsync(IEnumerable<int> positionIds);
        Task<IEnumerable<UserWithSummary>> GetAvailableSummariesFromFavorites(int userId);
        Task CreateFullSummaryAsync(SummaryModel summary, IEnumerable<int> educationIds, IEnumerable<int> workExperienceIds);
        Task UpdateFullSummaryAsync(SummaryModel summary, IEnumerable<int> educationIds, IEnumerable<int> workExperienceIds);
        Task<IEnumerable<SummaryModel>> GetAllByEducationIdAsync(int userId, int educationId);
        Task<IEnumerable<SummaryModel>> GetAllByWorkIdAsync(int userId, int workId);
        Task RemoveAllByUserIdAsync(int userId);
        Task HideSummaryAsync(int summaryId);
        Task RestoreSummaryAsync(int summaryId);
        Task AddToFavorites(int summaryId, int userId);
        Task RemoveFromFavorites(int summaryId, int userId);
    }
}

﻿using Bll.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bll.Interfaces
{
    /// <summary>
    /// Role logic interface 
    /// </summary>
    public interface IRoleService
    {
        Task<IEnumerable<RoleModel>> GetRolesForRegistrationAsync();
        Task<IEnumerable<RoleModel>> GetAllRolesAsync();
    }
}

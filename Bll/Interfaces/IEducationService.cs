﻿using Bll.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bll.Interfaces
{
    /// <summary>
    /// Education logic interface
    /// </summary>
    public interface IEducationService : ICrud<EducationModel>
    {
        Task<IEnumerable<EducationModel>> GetAllByUserIdAsync(int userId);
    }
}

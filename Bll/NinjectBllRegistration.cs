﻿using AutoMapper;
using Dal.Context;
using Dal.Uow;

namespace Bll
{
    public class NinjectBllRegistration:Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutomapperProfile());
            });

            Mapper mapper = new Mapper(mapperConfig);
            Bind<IMapper>().ToConstant(mapper).InSingletonScope();

            Bind<IUnitOfWork>().To<UnitOfWork>();
            Bind<UserContext>().ToSelf().WithConstructorArgument(typeof(string), "UserConnectionString");
            Bind<ApplicationContext>().ToSelf().WithConstructorArgument(typeof(string), "AppConnectionString");
        }
    }
}

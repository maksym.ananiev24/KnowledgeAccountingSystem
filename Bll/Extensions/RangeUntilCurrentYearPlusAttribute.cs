﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Bll.Extensions
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class RangeUntilCurrentYearPlusAttribute : RangeAttribute
    {
        public RangeUntilCurrentYearPlusAttribute(int minimum, int max) : base(minimum, DateTime.Now.AddYears(max).Year)
        {
        }
    }
}

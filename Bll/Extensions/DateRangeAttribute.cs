﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Bll.Extensions
{
    public class DateRangeAttribute:RangeAttribute
    {
        public DateRangeAttribute():base(typeof(DateTime), DateTime.Now.AddYears(-100).ToShortDateString(), DateTime.Now.ToShortDateString())
        {

        }
    }
}

﻿using System;
using System.Globalization;
using System.Security.Claims;
using System.Security.Principal;

namespace Bll.Extensions
{
    public static class IdentityExtensions
    {
        /// <summary>
        /// Get user id by identiry
        /// </summary>
        /// <param name="identity">user identity</param>
        /// <returns>user id</returns>
        public static int GetUserId(this IIdentity identity)
        {
            if (identity == null)
            {
                throw new ArgumentNullException("identity");
            }
            var ci = identity as ClaimsIdentity;
            if (ci != null)
            {
                var id = ci.FindFirst(ClaimTypes.NameIdentifier);
                if (id != null)
                {
                    return (int)Convert.ChangeType(id.Value, typeof(int), CultureInfo.InvariantCulture);
                }
            }
            return 0;
        }
        /// <summary>
        /// Get user role by identity
        /// </summary>
        /// <param name="identity">user identity</param>
        /// <returns>role id</returns>
        public static int GetUserRole(this IIdentity identity)
        {
            if (identity == null)
            {
                throw new ArgumentNullException("identity");
            }
            var ci = identity as ClaimsIdentity;
            int role=0;
            if (ci != null)
            {
                var id = ci.FindFirst(ClaimTypes.Role);
                if (id != null)
                    role = Convert.ToInt32(id.Value);
            }
            return role;
        }
    }
}

﻿using AutoMapper;
using Bll.Interfaces;
using Bll.Models;
using Dal.Entities;
using Dal.Uow;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Bll.Services
{
    /// <summary>
    /// Position logic implementation
    /// </summary>
    public class PositionService : IPositionService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public PositionService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<PositionModel> AddAsync(PositionModel model)
        {
            if (!_unitOfWork.PositionRepository.FindAll().Any(i => i.Name == model.Name))
            {
                var position=_unitOfWork.PositionRepository.Add(_mapper.Map<Position>(model));
                await _unitOfWork.SaveAsync();
                return _mapper.Map<PositionModel>(position);
            }
            else
            {
                throw new CustomException(nameof(model.Name),"Position already exists");
            }
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.PositionRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<PositionModel>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<PositionModel>>(await _unitOfWork.PositionRepository.FindAll().ToListAsync());
        }

        public async Task<PositionModel> GetByIdAsync(int id)
        {
            return _mapper.Map<PositionModel>(await _unitOfWork.PositionRepository.GetByIdAsync(id));
        }

        public async Task UpdateAsync(PositionModel model)
        {
            _unitOfWork.PositionRepository.Update(_mapper.Map<Position>(model));
            await _unitOfWork.SaveAsync();
        }
    }
}

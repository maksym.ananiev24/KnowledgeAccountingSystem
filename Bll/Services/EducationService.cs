﻿using AutoMapper;
using Bll.Interfaces;
using Bll.Models;
using Dal.Entities;
using Dal.Uow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Bll.Services
{
    /// <summary>
    /// Education logic implementation
    /// </summary>
    public class EducationService : IEducationService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public EducationService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<EducationModel> AddAsync(EducationModel model)
        {
            if(model.YearStart<=model.YearEnd)
            {
                var education = _unitOfWork.EducationRepository.Add(_mapper.Map<Education>(model));
                await _unitOfWork.SaveAsync();
                return _mapper.Map<EducationModel>(education);
            }
            throw new CustomException("EducationModel.YearStart", "Start Year cant be greate than End");
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            var education = await _unitOfWork.EducationRepository.GetByIdAsync(modelId);
            if (education != null)
            {
                await _unitOfWork.EducationRepository.DeleteByIdAsync(modelId);
                await _unitOfWork.SaveAsync();
                return;
            }
            throw new ArgumentException();
        }

        public async Task<IEnumerable<EducationModel>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<EducationModel>>(await _unitOfWork.EducationRepository.GetAllWithDetails().ToListAsync());
        }

        public async Task<IEnumerable<EducationModel>> GetAllByUserIdAsync(int userId)
        {
            return _mapper.Map<IEnumerable<EducationModel>>(await _unitOfWork.EducationRepository.GetAllWithDetails().Where(i=>i.UserId==userId).ToListAsync());
        }

        public async Task<EducationModel> GetByIdAsync(int id)
        {
            return _mapper.Map<EducationModel>(await _unitOfWork.EducationRepository.GetByIdWithDetailsAsync(id));
        }

        public async Task UpdateAsync(EducationModel model)
        {
            if (model.YearStart <= model.YearEnd)
            {
                _unitOfWork.EducationRepository.Update(_mapper.Map<Education>(model));
                await _unitOfWork.SaveAsync();
                return;
            }
            throw new CustomException("EducationModel.YearStart", "Start Year cant be greate than End");
        }
    }
}

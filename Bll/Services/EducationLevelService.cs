﻿using AutoMapper;
using Bll.Interfaces;
using Bll.Models;
using Dal.Entities;
using Dal.Uow;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Bll.Services
{
    /// <summary>
    /// Education level logic implementation
    /// </summary>
    public class EducationLevelService : IEducationLevelService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public EducationLevelService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<EducationLevelModel> AddAsync(EducationLevelModel model)
        {
            var educationLevel=_unitOfWork.EducationLevelRepository.Add(_mapper.Map<EducationLevel>(model));
            await _unitOfWork.SaveAsync();
            return _mapper.Map<EducationLevelModel>(educationLevel);
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.EducationLevelRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<EducationLevelModel>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<EducationLevelModel>>(await _unitOfWork.EducationLevelRepository.FindAll().ToListAsync());
        }

        public async Task<EducationLevelModel> GetByIdAsync(int id)
        {
            return _mapper.Map<EducationLevelModel>(await _unitOfWork.EducationLevelRepository.GetByIdAsync(id));
        }

        public async Task UpdateAsync(EducationLevelModel model)
        {
            _unitOfWork.EducationLevelRepository.Update(_mapper.Map<EducationLevel>(model));
            await _unitOfWork.SaveAsync();
        }
    }
}

﻿using AutoMapper;
using Bll.Interfaces;
using Bll.Models;
using Dal.Uow;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Bll.Services
{
    /// <summary>
    /// Role logic implementation
    /// </summary>
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public RoleService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<RoleModel>> GetAllRolesAsync()
        {
            return _mapper.Map<IEnumerable<RoleModel>>(await _unitOfWork.RoleRepository.FindAll().ToListAsync());
        }

        public async Task<IEnumerable<RoleModel>> GetRolesForRegistrationAsync()
        {
            return _mapper.Map<IEnumerable<RoleModel>>((await _unitOfWork.RoleRepository.FindAll().ToListAsync()).Skip(1));
        }
    }
}

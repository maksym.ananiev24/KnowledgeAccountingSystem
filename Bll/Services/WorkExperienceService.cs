﻿using AutoMapper;
using Bll.Interfaces;
using Bll.Models;
using Dal.Entities;
using Dal.Uow;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Bll.Services
{
    /// <summary>
    /// Work experience logic implementation
    /// </summary>
    public class WorkExperienceService : IWorkExperienceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public WorkExperienceService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<WorkExperienceModel> AddAsync(WorkExperienceModel model)
        {
            if(!model.FinishDate.HasValue || model.StartDate<=model.FinishDate)
            {
                var workExperience = _unitOfWork.WorkExperienceRepository.Add(_mapper.Map<WorkExperience>(model));
                await _unitOfWork.SaveAsync();
                return _mapper.Map<WorkExperienceModel>(workExperience);
            }
            throw new CustomException(nameof(model.StartDate), "Start Date cant be greate than End");
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.WorkExperienceRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<WorkExperienceModel>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<WorkExperienceModel>>(await _unitOfWork.WorkExperienceRepository.GetAllWithDetails().ToListAsync());
        }

        public async Task<IEnumerable<WorkExperienceModel>> GetAllByUserIdAsync(int userId)
        {
            return _mapper.Map<IEnumerable<WorkExperienceModel>>(await _unitOfWork.WorkExperienceRepository.GetAllWithDetails().Where(i => i.UserId == userId).ToListAsync());
        }

        public async Task<WorkExperienceModel> GetByIdAsync(int id)
        {
            return _mapper.Map<WorkExperienceModel>(await _unitOfWork.WorkExperienceRepository.GetByIdWithDetailsAsync(id));
        }

        public async Task UpdateAsync(WorkExperienceModel model)
        {
            if (!model.FinishDate.HasValue || model.StartDate <= model.FinishDate)
            {
                _unitOfWork.WorkExperienceRepository.Update(_mapper.Map<WorkExperience>(model));
                await _unitOfWork.SaveAsync();
                return;
            }
            throw new CustomException(nameof(model.StartDate), "Start Date cant be greate than End");
        }
    }
}

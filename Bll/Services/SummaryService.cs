﻿using AutoMapper;
using Bll.Interfaces;
using Bll.Models;
using Dal.Entities;
using Dal.Uow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Bll.Services
{
    /// <summary>
    /// Summary logic implementation
    /// </summary>
    public class SummaryService : ISummaryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public SummaryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<SummaryModel> AddAsync(SummaryModel model)
        {
            model.IsHidden = true;
            var summary = _unitOfWork.SummaryRepository.Add(_mapper.Map<Summary>(model));
            await _unitOfWork.SaveAsync();
            return _mapper.Map<SummaryModel>(summary);
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.SummaryRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<SummaryModel>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<SummaryModel>>(await _unitOfWork.SummaryRepository.GetAllWithDetails().ToListAsync());
        }

        public async Task<IEnumerable<SummaryModel>> GetAllByEducationIdAsync(int userId, int educationId)
        {
            return _mapper.Map<IEnumerable<SummaryModel>>(await _unitOfWork.SummaryRepository.GetAllWithDetails()
                .Where(i=>i.UserId==userId&&i.Educations.Any(inner=>inner.Id==educationId)).ToListAsync());
        }

        public async Task<IEnumerable<SummaryModel>> GetAllByUserIdAsync(int userId)
        {
            return _mapper.Map<IEnumerable<SummaryModel>>(await _unitOfWork.SummaryRepository.GetAllWithDetails().Where(i => i.UserId == userId).ToListAsync());
        }

        public async Task<IEnumerable<SummaryModel>> GetAllByWorkIdAsync(int userId, int workId)
        {
            return _mapper.Map<IEnumerable<SummaryModel>>(await _unitOfWork.SummaryRepository.GetAllWithDetails()
                .Where(i => i.UserId == userId && i.WorkExperiences.Any(inner => inner.Id == workId)).ToListAsync());
        }

        public async Task<SummaryModel> GetByIdAsync(int id)
        {
            var summary = await _unitOfWork.SummaryRepository.GetByIdWithDetailsAsync(id);
            if(summary!=null)
            {
                return _mapper.Map<SummaryModel>(summary);
            }
            throw new ArgumentException("Cant find summary with such Id");
        }

        public async Task RemoveAllByUserIdAsync(int userId)
        {
            var summaries = await _unitOfWork.SummaryRepository.FindAll().Where(i => i.UserId == userId).Select(i => i.Id).ToListAsync();
            if(summaries.Count!=0)
            {
                foreach (var item in summaries)
                {
                    await _unitOfWork.SummaryRepository.DeleteByIdAsync(item);
                }
                await _unitOfWork.SaveAsync();
            }
        }

        public async Task UpdateAsync(SummaryModel model)
        {
            _unitOfWork.SummaryRepository.Update(_mapper.Map<Summary>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task CreateFullSummaryAsync(SummaryModel summary, IEnumerable<int> educationIds, IEnumerable<int> workExperienceIds)
        {
            var summaryNew = _mapper.Map<Summary>(summary);
            if(educationIds!=null)
            foreach (var eId in educationIds.Distinct())
            {
                var education = await _unitOfWork.EducationRepository.GetByIdAsync(eId);
                if(education!=null)
                {
                    summaryNew.Educations.Add(education);
                }
                else
                {
                    throw new ArgumentException("Cant find education with such Id");
                }
            }
            if(workExperienceIds!=null)
            foreach (var wId in workExperienceIds.Distinct())
            {
                var workExperience = await _unitOfWork.WorkExperienceRepository.GetByIdAsync(wId);
                if (workExperience != null)
                {
                    summaryNew.WorkExperiences.Add(workExperience);
                }
                else
                {
                    throw new ArgumentException("Cant find workExperience with such Id");
                }
            }
            summaryNew.CreationDate = DateTime.Now;
            _unitOfWork.SummaryRepository.Add(summaryNew);
            await _unitOfWork.SaveAsync();
        }

        public async Task RestoreSummaryAsync(int summaryId)
        {
            var summary = await _unitOfWork.SummaryRepository.GetByIdAsync(summaryId);
            if (summary != null)
            {
                summary.IsHidden = false;
                _unitOfWork.SummaryRepository.Update(summary);
                await _unitOfWork.SaveAsync();
            }
            else
            {
                throw new ArgumentException("Cant find summary with such Id");
            }
        }

        public async Task HideSummaryAsync(int modelId)
        {
            var summary = await _unitOfWork.SummaryRepository.GetByIdAsync(modelId);
            if (summary != null)
            {
                summary.IsHidden = true;
                _unitOfWork.SummaryRepository.Update(summary);
                await _unitOfWork.SaveAsync();
            }
            else
            {
                throw new ArgumentException("Cant find summary with such Id");
            }
        }

        public async Task UpdateFullSummaryAsync(SummaryModel summary, IEnumerable<int> educationIds, IEnumerable<int> workExperienceIds)
        {
            var sum = await _unitOfWork.SummaryRepository.GetByIdWithDetailsAsync(summary.Id);
            sum.Description = summary.Description;
            sum.Educations.Clear();
            sum.WorkExperiences.Clear();
            if (educationIds != null)
            {
                foreach (var item in educationIds.Distinct())
                {
                    var education = await _unitOfWork.EducationRepository.GetByIdAsync(item);
                    if(education!=null)
                    {
                        sum.Educations.Add(education);
                    }
                }
            }
            if(workExperienceIds!=null)
            {
                foreach (var item in workExperienceIds.Distinct())
                {
                    var work = await _unitOfWork.WorkExperienceRepository.GetByIdAsync(item);
                    if(work!=null)
                    {
                        sum.WorkExperiences.Add(work);
                    }
                }
            }
            _unitOfWork.SummaryRepository.Update(sum);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<UserWithSummary>> GetAvailableSummariesByPositionIdAsync(IEnumerable<int> positionIds)
        {
            var usersWithSummaries = new List<UserWithSummary>();
            var users = await _unitOfWork.UserRepository.GetAllWithDetails().ToListAsync();
            foreach (var user in users)
            {
                var userModel = _mapper.Map<UserModel>(user);
                var userSummaries= await _unitOfWork.SummaryRepository.GetAllWithDetails().Where(i=>i.UserId==user.Id&&!i.IsHidden).ToListAsync();
                if(positionIds!=null)
                {
                    userSummaries = userSummaries.Where(i => positionIds.Contains(i.PositionId)).ToList();
                }
                foreach (var summary in userSummaries)
                {
                    usersWithSummaries.Add(new UserWithSummary()
                    {
                        UserModel=userModel,
                        SummaryModel=_mapper.Map<SummaryModel>(summary)
                    });
                }
            }
            return usersWithSummaries;
        }

        public async Task AddToFavorites(int summaryId, int userId)
        {
            var summary = await _unitOfWork.SummaryRepository.GetByIdWithDetailsAsync(summaryId);
            if (summary != null)
            {
                summary.EmployersId.Add(new EmployersInSummaries()
                {
                    SummaryId = summaryId,
                    EmployerId = userId
                });
                _unitOfWork.SummaryRepository.Update(summary);
                await _unitOfWork.SaveAsync();
                return;
            }
            throw new ArgumentException("Cant find summary with such Id");
        }

        public async Task RemoveFromFavorites(int summaryId, int userId)
        {
            var employerInSummary = _unitOfWork.EmployersInSummariesRepository.FindAll().FirstOrDefault(i => i.EmployerId == userId&&i.SummaryId==summaryId);
            if(employerInSummary!=null)
            {
                await _unitOfWork.EmployersInSummariesRepository.DeleteByIdAsync(employerInSummary.Id);
                await _unitOfWork.SaveAsync();
                return;
            }
            throw new ArgumentException("Cant find summary in your favorites");
        }

        public async Task<IEnumerable<UserWithSummary>> GetAvailableSummariesFromFavorites(int userId)
        {
            var usersWithSummaries = new List<UserWithSummary>();
            var users = await _unitOfWork.UserRepository.GetAllWithDetails().ToListAsync();
            foreach (var user in users)
            {
                var userModel = _mapper.Map<UserModel>(user);
                var userSummaries = await _unitOfWork.SummaryRepository.GetAllWithDetails().Where(i => i.UserId == user.Id && !i.IsHidden&&i.EmployersId.Select(inner=>inner.EmployerId).Contains(userId)).ToListAsync();
                foreach (var summary in userSummaries)
                {
                    usersWithSummaries.Add(new UserWithSummary()
                    {
                        UserModel = userModel,
                        SummaryModel = _mapper.Map<SummaryModel>(summary)
                    });
                }
            }
            return usersWithSummaries;
        }
    }
}

﻿using AutoMapper;
using Bll.Interfaces;
using Bll.Models;
using Dal.Entities;
using Dal.Uow;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Bll.Services
{
    /// <summary>
    /// User logic implementation
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task RegisterAsync(RegisterModel model)
        {
            var user = await _unitOfWork.UserRepository.GetUserByEmail(model.Email);
            if (user == null)
            {
                if(model.RoleId!=1)
                {
                    user = _mapper.Map<User>(model);
                    user.RoleId = model.RoleId;
                    user.Password = GetHashPassword(model.Password);
                    _unitOfWork.UserRepository.Add(user);
                    await _unitOfWork.SaveAsync();
                    return;
                }
                throw new CustomException(nameof(model.RoleId),"Cant use this role");
            }
            throw new CustomException(nameof(model.Email),"User with such Email already exists");
        }

        public async Task<ClaimsIdentity> AuthenticateAsync(LoginModel model)
        {
            var user = await _unitOfWork.UserRepository.GetUserByEmail(model.Email);
            if (user != null)
            {
                if (user.Password==GetHashPassword(model.Password))
                {
                    ClaimsIdentity claims = new ClaimsIdentity("ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
                    claims.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString(), ClaimValueTypes.String));
                    claims.AddClaim(new Claim(ClaimTypes.Role, user.RoleId.ToString(), ClaimValueTypes.String));
                    claims.AddClaim(new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email, ClaimValueTypes.String));
                    claims.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider",
                        "OWIN Provider", ClaimValueTypes.String));
                    claims.AddClaim(new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role.Name, ClaimValueTypes.String));
                    return claims;
                }
                throw new CustomException(nameof(model.Password), "Incorrect Password");
            }
            throw new CustomException(nameof(model.Email), "Cant find user with such Email");
        }

        public async Task<UserModel> GetUserByIdAsync(int Id)
        {
            return _mapper.Map<UserModel>(await _unitOfWork.UserRepository.GetByIdWithDetailsAsync(Id));
        }

        private string GetHashPassword(string password)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(password);

            MD5CryptoServiceProvider CSP =
                new MD5CryptoServiceProvider();

            byte[] byteHash = CSP.ComputeHash(bytes);

            StringBuilder hash = new StringBuilder();

            foreach (byte b in byteHash)
            {
                hash.Append(string.Format("{0:x2}", b));
            }
                
            return hash.ToString();
        }

        public async Task<IEnumerable<UserModel>> GetAllUsersAsync()
        {
            return _mapper.Map<IEnumerable<UserModel>>(await _unitOfWork.UserRepository.GetAllWithDetails().ToListAsync());
        }

        public async Task ChangeUserRoleAsync(int userId, int roleId)
        {
            var user = await _unitOfWork.UserRepository.GetByIdAsync(userId);
            if(user!=null)
            {
                var role = await _unitOfWork.RoleRepository.GetByIdAsync(roleId);
                if(role!=null)
                {
                    user.RoleId = roleId;
                    await _unitOfWork.SaveAsync();
                    return;
                }
                throw new ArgumentException("Cant find role with such Id");
            }
            throw new ArgumentException("Cant find user with such Id");
        }

        public async Task DeleteUserAsync(int userId)
        {
            var user=await _unitOfWork.UserRepository.GetByIdAsync(userId);
            if(user!=null)
            {
                await _unitOfWork.UserRepository.DeleteByIdAsync(userId);
                await _unitOfWork.SaveAsync();
                return;
            }
            throw new ArgumentException("Cant find user with such Id");
        }

        public async Task UpdateUserAsync(UserModel model)
        {
            if(model.Image!=null)
            {
                if(model.Image.ContentType.Contains("image"))
                {
                    using (var ms = new MemoryStream())
                    {
                        model.Image.InputStream.CopyTo(ms);
                        var fileBytes = ms.ToArray();
                        model.ProfileImage = Convert.ToBase64String(fileBytes);
                    }
                }
                else
                {
                    throw new CustomException(nameof(model.Image), "Only image available");
                }
            }
            _unitOfWork.UserRepository.Update(_mapper.Map(model, await _unitOfWork.UserRepository.GetByIdAsync(model.Id)));
            await _unitOfWork.SaveAsync();
        }
    }
}
